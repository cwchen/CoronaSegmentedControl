# [Corona] Segmented Control and Slider

A tiny demo application with segmented control and slider.

## Usage

Use [Git](https://git-scm.com/) to clone this repo:

```
$ git clone https://gitlab.com/cwchen/CoronaSegmentedControl.git
```

Alternatively, download the compressed repo as a zip file.

Then, open *main.lua* with [Corona](https://coronalabs.com/product/).

## Copyright

2018, Michael Chen; Apache 2.0
