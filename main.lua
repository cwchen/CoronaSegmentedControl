-----------------------------------------------------------------------------------------
--
-- main.lua
--
-----------------------------------------------------------------------------------------

-- Load widget package.
local widget = require("widget")

-- Init the parameters related to background color.
local red = 245
local green = 245
local blue = 220
local factor = 1

-- Declare the function to set background color.
local function setBackground(r, g, b, f)
  display.setDefault("background", r * f / 255, g * f / 255, b * f / 255)
end

-- Set initial background color.
setBackground(red, green, blue, factor)

-- Declare the listener for Segmented Control object.
local function onSegmentPress(event)
  local target = event.target

  if target.segmentNumber == 1 then
    -- Set RGB color to beige.
    red = 245
    green = 245
    blue = 220
  elseif target.segmentNumber == 2 then
    -- Set RGB color to skyblue.
    red = 135
    green = 206
    blue = 235
  elseif target.segmentNumber == 3 then
    -- Set RGB color lightgreen.
    red = 144
    green = 238
    blue = 144
  end

  setBackground(red, green, blue, factor)
end

-- Init a Segmented Control object.
local segmentedControl = widget.newSegmentedControl(
  {
    x = display.contentCenterX,
    y = display.contentHeight * 0.38,
    segmentWidth = 80,
    segments = { "Beige", "Sky Blue", "Light Green"},
    defaultSegment = 1,
    onPress = onSegmentPress
  }
)

-- Declare the listener for Slider object.
local function sliderListener(event)
  if event.phase == "moved" then
    factor = ((100 - event.value) * 0.2 + 80) / 100
    setBackground(red, green, blue, factor)
  end
end


-- Init a Slider object.
local slider = widget.newSlider(
  {
    x = display.contentCenterX,
    y = display.contentHeight * 0.47,
    width = display.contentWidth * 0.75,
    value = 0,
    listener = sliderListener,
  }
)
